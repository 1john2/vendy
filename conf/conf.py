"""
filename: conf/selectors.py
project: Vendy
Author: Malanik Jan
Email: malanik(dot)jan(at)gmail(dot)com
Description:
    define basic constants
"""

ITER = 20
WIKI_HOME = "https://en.wikipedia.org/wiki/Main_Page"
TIMEOUT = 1
