"""
filename: __init__.py
project: Vendy
Author: Malanik Jan
Email: malanik(dot)jan(at)gmail(dot)com
Description:

Let's play with selenium and Wiki
We've heard that all roads lead to Philosophy.
Write a simple piece of code to prove it.
• Open random article on Wikipedia
• Click the first link in the article until you get to the page about Philosophy
• Count and print out the number of redirects (transitions)
You can use any OOP language. However, you have to use Selenium and Page Object
Pattern (https://martinfowler.com/bliki/PageObject.html,https://github.com/SeleniumHQ/selenium/w
iki/PageObjects ).
"""
